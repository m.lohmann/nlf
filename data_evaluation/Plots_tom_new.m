 set(groot, 'defaultAxesTickLabelInterpreter','none');
% set(groot, 'defaultLegendInterpreter','latex');
 set(groot, 'defaultTextInterpreter','none');
% 
% 
% set(0,'defaultfigureposition',[610   428   700   525])
set(0,'defaultfigureposition',[610   428   900   525])
set(0,'defaultaxesfontsize',9);
set(0,'defaulttextfontsize',9); 


%% Output Laser diode (abs. units)


DataDiode=csvread('laserdiode_characterization.csv',1,0);
DataDiode(:,3)=DataDiode(:,3)*392/interp1(DataDiode(:,1),DataDiode(:,3),550); %calibration

z0=1:8;
z1=9:15;
z2=1:29;
z2=setdiff(z2,[z0,z1]);
func=@(a,i0,x)a*(x-i0);
regreff=fit(DataDiode(z2,1),DataDiode(z2,3),func,'StartPoint',[0.8,50])



%%
coeff=table2array(regre.Coefficients);
offs=coeff(1,1);
offs_err=coeff(1,2);
m=coeff(2,1);
m_err=coeff(2,2);
onset=-offs/m;
onset_err=sqrt((offs_err*onset/offs)^2+(m_err*onset/m)^2);
xxx=DataDiode(z1,1);
xxx_l=linspace(0,600,601);
regerr=regre.RMSE*sqrt(1/length(xxx) + ((xxx_l-mean(xxx)).^2)/sum((xxx-mean(xxx)).^2));
x=xxx_l;
y=offs+m*x;

ons_err=regerr(150);
z2=1:29;
z2=setdiff(z2,[z0,z1]);


regre2=fitlm(DataDiode(z2,1),DataDiode(z2,3));
coeff2=table2array(regre2.Coefficients);
offs2=coeff2(1,1);
offs_err2=coeff2(1,2);
m2=coeff2(2,1);
m_err2=coeff2(2,2);
onset2=-offs2/m2;
xxx2=DataDiode(z1,1);
xxx_l2=linspace(0,600,601);
regerr2=regre2.RMSE*sqrt(1/length(xxx2) + ((xxx_l2-mean(xxx2)).^2)/sum((xxx2-mean(xxx2)).^2));
x2=xxx_l2;
y2=offs2+m2*x2;




h=figure;
hold on, box on
dcolor=[[0,0,0.8];[0.8,0,0];[0,0,0];[0,0.3,0]];
plot(x,y,'Color',dcolor(2,:));
fi=fill([x,flip(x)],[y-regerr, flip(y+regerr)],dcolor(2,:));
set(fi,'facealpha',0.2,'edgecolor','none');


plot(x2,y2,'Color',dcolor(1,:))
fi2=fill([x2,flip(x2)],[y2-regerr2,flip(y2+regerr2)],dcolor(1,:));
set(fi2,'facealpha',0.5,'edgecolor','none');

% errorbar(DataDiode(z0,1),DataDiode(z0,3),0.1*ones(size(z0)),'ko','MarkerFaceColor','k')
diodep(1)=plot(DataDiode(z0,1),DataDiode(z0,3),'o','MarkerFaceColor',dcolor(3,:),'Color',dcolor(3,:));
diodep(2)=plot(DataDiode(z1,1),DataDiode(z1,3),'s','MarkerFaceColor',dcolor(2,:),'Color',dcolor(2,:));
diodep(3)=plot(DataDiode(z2,1),DataDiode(z2,3),'o','MarkerFaceColor',dcolor(1,:),'Color',dcolor(1,:));

xlim([0 600])
ylim([-30 430])
xlabel('Diode current [mA]')
ylabel('Output power [mW]')
% title('Laser diode')
text(350,30,sprintf('Diode temperature %.1f�C',DataDiode(1,2)))
text(40,300,sprintf('Threshold:\n%.0f mA',round(onset,0)),'color',dcolor(4,:),'HorizontalAlignment','Left')
text(330,250,sprintf('%.2f W/A',m),'color',dcolor(2,:),'rotation',45)
text(440,270,sprintf('%.2f W/A',m2),'color',dcolor(1,:),'rotation',30)
onsline=line(onset*[1 1],[-30,430]);
set(onsline,'Color',dcolor(4,:),'LineStyle',':','LineWidth',1.5);

set(diodep,'MarkerSize',3.5);
set(h,'Units','centimeters');
set(0,'defaultaxesfontsize',8.5);
set(0,'defaulttextfontsize',8.5); 
set(h,'Position',[6,6,14,7]);
grid on;
set(gca,'YTick',0:100:500)
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[1.01*pos(3), 1.01*pos(4)])
print(h,'.\protocol_thomas\figures\diode_charac','-dpdf','-r600')


%% Temperature dependence of laser diode from absorption measurements
Abs250mA=csvread('TempDependence250mA.csv',1,0);
Abs400mA=csvread('TempDependence400mA.csv',1,0);
Abs550mA=csvread('TempDependence550mA.csv',1,0);

% Abs250mA(:,2)=Abs250mA(:,2)/5; % gain factor changed

%Laser diode power corrected
% Abs400mA(:,2)=Abs400mA(:,2)/1.5;
% Abs550mA(:,2)=Abs550mA(:,2)/3;

c=lines(3);

figure
hold on, box on
plot(Abs250mA(:,1),Abs250mA(:,2),'d-','color',c(1,:),'MarkerFaceColor',c(1,:))
plot(Abs400mA(:,1),Abs400mA(:,2),'o-','color',c(2,:),'MarkerFaceColor',c(2,:))
plot(Abs550mA(:,1),Abs550mA(:,2),'s-','color',c(3,:),'MarkerFaceColor',c(3,:))
xlim([35 45])
legend({'250mA','400mA','550mA'},'location','best')
xlabel('Temperature (�C)')
ylabel('Intensity (arb.u.)')
title('Absorption measurement')

extrema=[26 37 43];
colorExtrema=[[0,0.4,0.1];[0,0.4,0.1];[0,0.4,0.1]]*255;

%%


Abs250mA=csvread('TempDependence250mA.csv',1,0);
Abs400mA=csvread('TempDependence400mA.csv',1,0);
Abs550mA=csvread('TempDependence550mA.csv',1,0);

Abs250mA(:,2)=Abs250mA(:,2)/2; % gain factor changed

P0 = interp1(DataDiode(:,1),DataDiode(:,3),[250 400 550]);
P_rel=P0/P0(end);
%Laser diode power corrected
Abs250mA(:,2)=Abs250mA(:,2)/P_rel(1)/6000;
Abs400mA(:,2)=Abs400mA(:,2)/P_rel(2)/6000;
Abs550mA(:,2)=Abs550mA(:,2)/6000;
% Abs550mA(:,2)=Abs550mA(:,2)/3;
Abs550mA(find(Abs550mA(:,1)<36),:)=[];
c=lines(3);
c=[0.8*[1 0 0];0.8*[0 0 1];0.9*[1 0.7 0]];

h=figure;
hold on, box on
ppk(1)=plot(Abs250mA(:,1),Abs250mA(:,2),'d-','color',c(3,:),'MarkerFaceColor',c(3,:))
ppk(2)=plot(Abs400mA(:,1),Abs400mA(:,2),'o-','color',c(2,:),'MarkerFaceColor',c(2,:))
ppk(3)=plot(Abs550mA(:,1),Abs550mA(:,2),'s-','color',c(1,:),'MarkerFaceColor',c(1,:))
xlim([35.7 45.3])
set(ppk,'MarkerSize',3.5);
leg=legend({'250 mA','400 mA','550 mA'},'location','best')
set(leg,'box','off');
xlabel('Diode Temperature (�C)')
ylabel('Normalized transmitted power (a.u.)')
% title('Absorption measurement')
set(leg,'Position',[0.66,0.7,0.3,0.2]);
set(h,'Units','centimeters');
set(0,'defaultaxesfontsize',8.5);
set(0,'defaulttextfontsize',8.5); 
set(h,'Position',[6,6,14,7]);
grid on;
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[1.01*pos(3), 1.01*pos(4)])
print(h,'.\protocol_thomas\figures\absorption_current_dep','-dpdf','-r600')




%%
colorExtrema=[[0,0.3,0];[0,0.3,0];[0.7,0,0]]*1;
h=figure;
hold on, box on
Abs550mA=csvread('TempDependence550mA.csv',1,0);
Abs550mA(:,2)=Abs550mA(:,2)/1000;
ab_pl=plot(Abs550mA(:,1),Abs550mA(:,2),'.-','color','k','MarkerFaceColor','k');
for ii=1:3
    line(extrema(ii)*[1 1], [0 0.500],...
        'color',colorExtrema(ii,:),'linestyle','--','clipping','off')
    text(extrema(ii)-3,0.580,sprintf('%.0f�C',extrema(ii)),'color',colorExtrema(ii,:))
end
xlabel('Temperature [�C]')
ylim([0 1.150]);
xlim([4 45]);
ylabel('Transmitted power [a.u.]')
% title('Absorption measurement')
set(h,'Units','centimeters');
set(0,'defaultaxesfontsize',8.5);
set(0,'defaulttextfontsize',8.5); 
set(h,'Position',[6,6,8.5,5.8]);
set(ab_pl,'MarkerSize',9);
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[1.01*pos(3), 1.01*pos(4)])
print(h,'.\protocol_thomas\figures\absorption','-dpdf','-r600')

%% Fluorescence measurement
set(0,'defaultaxesfontsize',8.5);
set(0,'defaulttextfontsize',8.5); 
fluorescenceOff=csvread('fluorescenceOff.csv');
fluorescenceOff2=csvread('fluorescenceOff2.csv');
fluorescenceOn=csvread('fluorescenceOn.csv');

offLimits=[19.2 -20.4];
onLimits=[-18 21.2];
%fluorescenceOff(:,1)=fluorescenceOff(:,1)-40;
fluorescenceOff(:,2)=fluorescenceOff(:,2)-min(offLimits);
fluorescenceOff2(:,2)=fluorescenceOff2(:,2)-min(offLimits);
fluorescenceOn(:,2)=fluorescenceOn(:,2)-min(onLimits);
offAll=cat(1,fluorescenceOff,fluorescenceOff2);

expDecay = @(a,b,c,x) a*exp(-1/b*x)+c;
expDecay4= @(a,b,x) expDecay(a,b,0,x);
expDecay2= @(b,x) expDecay(abs(diff(offLimits)),b,0,x);
expDecay3= @(a,b,x) expDecay(a,b,abs(diff(onLimits)),x);
expDecay5= @(a,b,x) expDecay(a,b,abs(diff(onLimits)),x);
%fOff=fit(fluorescenceOff(:,1),fluorescenceOff(:,2),expDecay2,'StartPoint',[230])%
%fOff2=fit(fluorescenceOff2(:,1),fluorescenceOff2(:,2),expDecay4,'StartPoint',[abs(diff(offLimits)),50])%[40 250 0])
fOn=fit(fluorescenceOn(:,1),fluorescenceOn(:,2),expDecay3,'StartPoint',[-abs(diff(onLimits)),250])%[40 250 0])
fOffAll=fit(offAll(:,1),offAll(:,2),expDecay4,'StartPoint',[abs(diff(offLimits)),230])

lifetime=[fOffAll.b fOn.b];
x=0:10:600;

colR=[0.8,0,0];
colG=[0,0.4,0];

c=lines(3);
fig_lifetime=figure;
hold on,box on
xlabel('Time [�s]')
ylabel('Photodiode voltage [mV]')
fluon_err=repmat([2],1,length(fluorescenceOn(:,1)));
fluoff_err=repmat([2],1,length(fluorescenceOff(:,1)));
fluoff_err2=repmat([2],1,length(fluorescenceOff2(:,1)));
p_on=errorbar(fluorescenceOn(:,1),fluorescenceOn(:,2),fluon_err,'.','Color',colG,'MarkerFaceColor',colG);
p_off=errorbar(fluorescenceOff(:,1),fluorescenceOff(:,2),fluoff_err,'s','Color',colR,'MarkerFaceColor',colR);
p_off2=errorbar(fluorescenceOff2(:,1),fluorescenceOff2(:,2),fluoff_err2,'d','Color',colR,'MarkerFaceColor',colR);
ylim([-3 43]);

set(p_on,'MarkerSize',13);
set(p_off,'MarkerSize',4);
set(p_off2,'MarkerSize',4);
text(230,6,'\tau = 252 �s','color',colR,'interpreter','tex');
text(230,34,'\tau = 235 �s','color',colG,'interpreter','tex');


plot(x,feval(fOn,x),'color',colG)
plot(x,feval(fOffAll,x),'color',colR)
[asd,lege]=legend({'Turn on','Turn off #1','Turn off #2'},'location','best','box','off');
set(asd,'Position',[0.75,0.45,0.1,0.2]);

%legt=findobj(leg,'type','line');
grid on;
set(fig_lifetime,'Units','centimeters');
set(fig_lifetime,'Position',[6,6,14,7]);
pos = get(fig_lifetime,'Position');
set(fig_lifetime,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[1.01*pos(3), 1.01*pos(4)])
print(fig_lifetime,'.\protocol_thomas\figures\fluorescence','-dpdf','-r600')
% figure
% semilogy(fluorescenceOff(:,1),fluorescenceOff(:,2)+16)
% hold on
% semilogy(fluorescenceOff2(:,1),fluorescenceOff2(:,2)+16)
% semilogy(fluorescenceOn(:,1),18-fluorescenceOn(:,2))

%% Temp. dependence Nd:YAG laser
LaserTempDep=csvread('LaserTempDependence_edit.csv',1,0);
extrema=[27 38 44];
colorExtrema={0.3*[0 1 0],0.3*[0 1 0],0.7*[1 0 0]};
tcolor=[0 0 0.4];
h=figure;
ptk=plot(LaserTempDep(:,1),LaserTempDep(:,2),'o-','MarkerFaceColor',tcolor,'Color',tcolor)
for ii=1:3
    line(extrema(ii)*[1 1], [20 LaserTempDep(LaserTempDep(:,1)==extrema(ii),2)],...
        'color',colorExtrema{ii},'linestyle','--','clipping','off')
    text(extrema(ii)-3,35,sprintf('%.0f�C',extrema(ii)),'color',colorExtrema{ii})
end
set(ptk,'MarkerSize',3.5);
ylim([20 110])
xlabel('Diode Temperature (�C)')
ylabel('Output Power (mW)')

set(h,'Units','centimeters');
set(0,'defaultaxesfontsize',8.5);
set(0,'defaulttextfontsize',8.5); 
set(h,'Position',[6,6,14,7]);

grid on;
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[1.01*pos(3), 1.01*pos(4)])
print(h,'.\protocol_thomas\figures\laser_temp_dep','-dpdf','-r600')


% hold on
% plot(Abs550mA(:,1),(max(Abs550mA(:,2))-Abs550mA(:,2))/10,'rs-','MarkerFaceColor','r')





%% Output Nd:Yag laser (absolute units)
close all;
Data1064=csvread('LaserClassification.csv',1,0);
slope=@(a,i0,x)(a*(x-i0));
thresh_relevant=(length(Data1064)-5:length(Data1064)-1);
Data1064(end-1,:);
p1064=polyfit(Data1064(thresh_relevant,1),Data1064(thresh_relevant,3),1);
p1064_2=fit(Data1064(thresh_relevant,1),Data1064(thresh_relevant,3),slope,'StartPoint',[0.3,200]);
x=[180 300];
x2=[400 600];
y=polyval(p1064,x);
y2=polyval(p1064,x2);
ndyag.threshold=-p1064(2)/p1064(1);
coeff_err=confint(p1064_2);
p1064_all=polyfit(Data1064(1:end-1,1),Data1064(1:end-1,3),1);
p1064_all2=fit(Data1064(1:end-1,1),Data1064(1:end-1,3),slope,'StartPoint',[0.3,200])
x_all=[180 700];
y_all=polyval(p1064_all,x_all);

h=figure;
hold on

temp_color=[0.3,0.3,0.9];
temp_ax_color=[0,0,0.7];
linecolor=[0,0.3,0];
threshcolor=[0.9,0,0];
grid on;
box on;
yyaxis left;
ax1=gca;
% avoid box border being overwritten by plot lines...
ylim(ax1,[-5 120]);
p_all=plot(x_all,y_all,'-','color',0.8*[1 1 1],'linew',3);
text(500,75,sprintf('%.2f W/A',p1064_all(1)),'color',0.5*[1 1 1],'rotation',30)

hold on;
p_tf=plot(x,y,'linew',1.5,'Color',threshcolor)
onsline=line(ndyag.threshold*[1 1],[-5,120]);
p_power=plot(ax1,Data1064(:,1),Data1064(:,3),'ko','MarkerFaceColor','k','MarkerSize',3.5);
%set(p_power,'MarkerSize',15);

hold on;

ax1.YColor=[0 0 0];
xlabel(ax1,'Diode Current (mA)');
ylabel(ax1,'Output Power (mW)');

set(onsline,'Color',linecolor,'LineStyle',':','LineWidth',1.5);
text(220,30,sprintf('Threshold:\n%.0f mA',round(ndyag.threshold,0)),...
    'color',linecolor,'HorizontalAlignment','Left');


yyaxis right;
p_temp=plot(ax1,Data1064(:,1),Data1064(:,2),'d');
set(p_temp,'Color',temp_color);
set(p_temp,'MarkerFaceColor',temp_color);
set(p_temp,'MarkerSize',3.5);
ylim([35.75 42]);
yticks([37,39,41]);
ylabel(ax1,'Diode Temperature (�C)');
ax1.YColor=temp_ax_color;
%set(p_temp,'alpha',0.1);
%text(500,75,sprintf('%.2f W/A',p1064_all(1)),'color',0.7*[1 1 1],'rotation',37)
[leg,icons,plots,txt] =legend([p_power,p_temp,p_all,p_tf],{'Output Power','Set diode temperature','Overall linear fit','Fit for Threshold Determination'});

p1 = icons(1).Position;
%
for uk=[1:4];
icons(uk).Position(1) = 0.22 ;
end
for uk=[9,11];
icons(uk).XData=[0.075 0.175];
end
set(leg,'box','off');
set(leg,'Position',[0.28,0.66,0.4,0.25]);
posAx=get(gca,'position');
xlim([170,600]);
set(gca,'Layer','Top');
set(gca,'Clipping','on');

set(h,'Units','centimeters');
set(0,'defaultaxesfontsize',8.5);
set(0,'defaulttextfontsize',8.5); 
set(h,'Position',[6,6,14,7]);

grid on;
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[1.01*pos(3), 1.01*pos(4)])
print(h,'.\protocol_thomas\figures\laser_charac','-dpdf','-r600')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%  
figure
plot(interp1(DataDiode(:,1),DataDiode(:,3),Data1064(:,1)),Data1064(:,3),'ks','MarkerFaceColor','k')
xlabel('Pump power (mW)')
ylabel('Output power (mW)') 
%%


fTemp=fit(Data1064(1:18,1),Data1064(1:18,2),'poly1')
x=199:601;
regerr_te=predint(fTemp,x,0.95,'functional','off');
h=figure;
hold on, box on
bcolor=[0,0,0.7];
ggcolor=0.3*[1,1,1];
rcolor=0.8*[1,0,0];
% plot(x,y,'r','linew',1.5)


p_ts(1)=plot(Data1064(1:18,1),Data1064(1:18,2),'o','MarkerFaceColor',bcolor);
p_ts(2)=plot(Data1064(19:20,1),Data1064(19:20,2),'o','MarkerFaceColor',ggcolor);
p_rt=plot(fTemp);
%p_test=plot(x,regerr_te(:,1));
freg=fill([x,flip(x)]',[regerr_te(:,1); flip(regerr_te(:,2))],rcolor);
set(freg,'facealpha',0.2,'edgecolor','none');
uistack(p_ts(1),'top');
xlim([200,600]);
set(p_rt,'color',rcolor);
legend off;
set(p_ts(1),'Color',bcolor);
set(p_ts(2),'Color',ggcolor);
set(p_ts,'MarkerSize',3.5);
xlabel('Diode Current (mA)')
ylabel('Optimized Temp. (�C)')

set(h,'Units','centimeters');
set(0,'defaultaxesfontsize',8.5);
set(0,'defaulttextfontsize',8.5); 
set(h,'Position',[6,6,8.5,5]);
grid on;
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[1.01*pos(3), 1.01*pos(4)])
print(h,'.\protocol_thomas\figures\laser_temp_shift','-dpdf','-r600')



%% Output green Nd:Yag laser (absolute units)
    DataSHG=csvread('SHG.csv',1,0);
    outlyer=DataSHG(:,1)>480;

    SHGsq=sqrt(DataSHG(1:end-4,2));
    xx=DataSHG(1:end-4,1);
    nlfunct=@(a,b,x)(a*(x-b).^2);
    [pSHG,fitex2,fitex3]=fit(DataSHG(:,1),DataSHG(:,2),nlfunct,'StartPoint',[1 200],'Exclude',outlyer);
    coeff_con=confint(pSHG,0.95)
    a_low=coeff_con(1,1);
    a_high=coeff_con(2,1);
    b_low=coeff_con(1,2);
    b_high=coeff_con(2,2);
    coval=coeffvalues(pSHG);
    %nlub=@(a,x)nlfunct(a,b_low);
    blSHG=fit(DataSHG(:,1),DataSHG(:,2),@(a,x)nlfunct(a,b_low,x),'StartPoint',1,'Exclude',outlyer);
    ulSHG=fit(DataSHG(:,1),DataSHG(:,2),@(a,x)nlfunct(a,b_high,x),'StartPoint',1,'Exclude',outlyer);
    ulcoval=coeffvalues(ulSHG);
    blcoval=coeffvalues(blSHG);
    model= @(b,x) (b(1)*(x-b(2)).^2);
    [nl_beta,nl_R,nl_J]=nlinfit(DataSHG(1:end-4,1),DataSHG(1:end-4,2),model,[1 200]);
    x=140:580;
    [y_nl,delta_nl]=nlpredci(model,x,nl_beta,nl_R,'Jacobian',nl_J);
    
    p11=predint(pSHG,x,0.95,'functional','off');%+0.0117;;
    [~,bd]=min(p11(:,2));
    tmp=p11(1:bd,1);
    p11(1:bd,1)=p11(1:bd,2);
    p11(1:bd,2)=tmp;
    %p11(:,1)=p11(:,1)+abs(min(p11(:,1)));
    %p11(:,2)=p11(:,2)+abs(min(p11(:,2)));
    lbfunct=@(x)nlfunct(blcoval(1),b_low,x);
    ubfunct=@(x)nlfunct(ulcoval(1),b_high,x);
    %ubfunct=@(x)nlfunct(coval(1),
    ub_cb=p11(:,2);
    ub_cb(ub_cb<0)=0;
    lb_cb=p11(:,1);
    lb_cb(lb_cb<0)=0;
    y_lb=feval(lbfunct,x);
    y_ub=feval(ubfunct,x);
    %y_lb=min(y_lb,lb_cb');
    %y_ub=max(y_ub,ub_cb');
    y=feval(pSHG,x);
    y=y(x>coval(2));
    y_ub(x>270)=lb_cb(x>270);
    y_lb(x>260)=ub_cb(x>260);
    y_lb=y_lb(x>b_low);
    y_ub=y_ub(x>b_high);
    close all;
    greencolor=[0,0.5,0];
    h=figure(38);
    hold on, box on
    onsline=line(coval(2)*[1 1],[-0.1,5]);
    set(onsline,'Color',greencolor,'LineStyle',':','LineWidth',1.5);
    fi_tr=fill([b_low,b_high,b_high,b_low,b_low],[-0.1 -0.1 5 5 -0.1],greencolor);
    set(fi_tr,'facealpha',0.1,'edgecolor','none');
    %plot(DataSHG(:,1),DataSHG(:,2),'ks','MarkerFaceColor','k')
    gcolor=[0.3,0.3,0.6];
    rcolor=[0.8,0,0];
    plot(x(x>coval(2)),y,'-','Color',rcolor,'LineWidth',1); 
    p_shg=errorbar(DataSHG(1:end-4,1),DataSHG(1:end-4,2),0.1*DataSHG(1:end-4,2),'.k');
    p_shg_out=errorbar(DataSHG(outlyer,1),DataSHG(outlyer,2),0.1*DataSHG(outlyer,2),'d','Color',gcolor,'MarkerFaceColor',gcolor);
    set(p_shg_out,'MarkerSize',3);
    set(p_shg,'MarkerSize',11);
    
    fishg=fill([x(x>b_high),flip(x(x>b_low))],[y_ub,flip(y_lb)],rcolor);
    set(fishg,'facealpha',0.15,'edgecolor','[0.8,0,0]','LineWidth',0.1);    
    
   
    
    %plot(x(x>b_high),y_ub,'r-');
    %plot(x(x>b_low),y_lb,'r-');
    %plot(x,lb_cb,'g-');
    %plot(x,ub_cb,'g-');
    %plot(x,p11(:,1),'g-');
    %plot(x,p11(:,2),'g-');
    %plot(x,sa,'b-');
    xlim([160 560])
    ylim([-0.01 3.4]);
    posAx=get(gca,'position');
    xl=xlim;
    %annotation(h,'textarrow',posAx(1)+posAx(3)*(pSHG.b-xl(1))/diff(xl)*[1 1],[0.30 posAx(2)],...
        %'String',{'Threshold:',sprintf('%.0f mA',pSHG.b)},'color','k','HorizontalAlignment','center');
    xlabel('Pump diode current I [mA]','interpreter','tex')
    ylabel('SHG output power P_{SHG} [mW]')
    % title('Frequency doubled Nd:YAG output')
    text(245,2.5,sprintf('Threshold:\n(%.0f \\pm 22) mA',round(coval(2))),'color',[0 0.3 0],'HorizontalAlignment','Left','Interpreter','tex')
set(h,'Units','centimeters');
set(0,'defaultaxesfontsize',8.5);
set(0,'defaulttextfontsize',8.5); 
set(h,'Position',[6,6,14,7]);
%grid on;
%set(gca,'YTick',0:100:500)
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[1.01*pos(3), 1.01*pos(4)])
print(h,'.\protocol_thomas\figures\shg_charac','-dpdf','-r600')

    %%
    fSHG2=fit(interp1(DataDiode(:,1),DataDiode(:,3),DataSHG(:,1)),DataSHG(:,2),'a*(x-b)^2','StartPoint',[1 0.3]);
    x2=100:400;
    y2=feval(fSHG2,x2);

    figure
    hold on, box on
    plot(interp1(DataDiode(:,1),DataDiode(:,3),DataSHG(:,1)),DataSHG(:,2),'ks','MarkerFaceColor','k')
    plot(x2,y2,'k-')
    xlabel('Pump power (mW)')
    ylabel('SHG output (mW)')
    % title('Frequency doubled Nd:YAG output')

    % figure
    % loglog(DataSHG(:,1)-240,DataSHG(:,2),'ks-','MarkerFaceColor','k')
    % xlabel('Current (mA)')
    % ylabel('SHG output (mW)')
