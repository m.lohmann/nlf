function [t,n,q]=LaserRateEquations_tom(b,p,dt,tmax)
T=[dt:dt:tmax];
[t,y]=ode45(@rateEquations,T,1e-6*[1 0]);
q=y(:,1);
n=y(:,2);


    function dy=rateEquations(t,y)
        dy=zeros(2,1);
        dy(1)=y(1)*(y(2)-1);
        dy(2)=p-b*y(2)-2*y(1)*y(2);
    end
end