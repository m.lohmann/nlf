% set(groot, 'defaultAxesTickLabelInterpreter','latex');
% set(groot, 'defaultLegendInterpreter','latex');
% set(groot, 'defaultTextInterpreter','none');
% 
% 
% set(0,'defaultfigureposition',[610   428   700   525])
set(0,'defaultfigureposition',[610   428   900   525])
set(0,'defaultfigureposition',[610   428   800   466])
%% Output Laser diode (abs. units)
DataDiode=csvread('laserdiode_characterization.csv',1,0);
DataDiode(:,3)=DataDiode(:,3)*392/interp1(DataDiode(:,1),DataDiode(:,3),550); %calibration

z0=1:8;
z1=9:15;
p=fit(DataDiode(z1,1),DataDiode(z1,3),'a*(x-b)');
onset=p.b;
p_confInt=confint(p,0.68);
onset_err=p_confInt(:,2)-p.b;
onsetDiode=[onset,max(onset_err)];
x=[100 550];
y=feval(p,x);

z2=1:29;
z2=setdiff(z2,[z0,z1]);
p2=polyfit(DataDiode(z2,1),DataDiode(z2,3),1);
x2=[30 600];
y2=polyval(p2,x2);

h=figure;
hold on, box on
% errorbar(DataDiode(z0,1),DataDiode(z0,3),0.1*ones(size(z0)),'ko','MarkerFaceColor','k')
plot(DataDiode(z0,1),DataDiode(z0,3),'ko','MarkerFaceColor','k')
plot(DataDiode(z1,1),DataDiode(z1,3),'rs','MarkerFaceColor','r')
plot(DataDiode(z2,1),DataDiode(z2,3),'bo','MarkerFaceColor','b')
plot(x,y,'r')
plot(x2,y2,'b')
% line(p_confInt,[0 0],'color','r')
xlim([0 600])
ylim([-30 450])
xlabel('Diode current (mA)')
ylabel('Output power (mW)')
% title('Laser diode')
text(30,420,sprintf('Diode temperature %.1f�C',DataDiode(1,2)))
text(180,0,sprintf('Threshold: (%.0f \\pm %.0f) mA',onsetDiode),'color','r')
text(330,250,sprintf('%.2f W/A',p.a),'color','r','rotation',45)
text(440,270,sprintf('%.2f W/A',p2(1)),'color','b','rotation',30)
set(gca,'YTick',0:100:500)
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\LaserDiode','-dpdf','-r600')


%% Temperature dependence of laser diode from absorption measurements
Abs250mA=csvread('TempDependence250mA.csv',1,0);
Abs400mA=csvread('TempDependence400mA.csv',1,0);
Abs550mA=csvread('TempDependence550mA.csv',1,0);

Abs250mA(:,2)=Abs250mA(:,2)/2; % gain factor changed

P0 = interp1(DataDiode(:,1),DataDiode(:,3),[250 400 550]);
P_rel=P0/P0(end);
I0=1100;
%Laser diode power corrected
Abs250mA(:,2)=Abs250mA(:,2)/P_rel(1)/I0;
Abs400mA(:,2)=Abs400mA(:,2)/P_rel(2)/I0;
Abs550mA(:,2)=Abs550mA(:,2)/I0;

c=lines(3);
c=[[1 0 0];[0 0 1];[0 0 0]];

h=figure;
hold on, box on
plot(Abs250mA(:,1),Abs250mA(:,2),'d-','color',c(1,:),'MarkerFaceColor',c(1,:))
plot(Abs400mA(:,1),Abs400mA(:,2),'o-','color',c(2,:),'MarkerFaceColor',c(2,:))
plot(Abs550mA(:,1),Abs550mA(:,2),'s-','color',c(3,:),'MarkerFaceColor',c(3,:))
xlim([35 45])
legend({'250mA','400mA','550mA'},'location','best')
xlabel('Temperature (�C)')
ylabel('Rel. intensity (arb.u.)')
% title('Absorption measurement')
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\figures\AbsorptionDifferentCurrents','-dpdf','-r600')

extrema=[26 37 43];
colorExtrema=['r','r','b'];

h=figure;
hold on, box on
plot(Abs550mA(:,1),Abs550mA(:,2),'s','color','k','MarkerFaceColor','k')
for ii=1:3
    line(extrema(ii)*[1 1], [1.2 Abs550mA(Abs550mA(:,1)==extrema(ii),2)],...
        'color',colorExtrema(ii),'linestyle','--','clipping','off')
    text(extrema(ii)-2.7,0.8,sprintf('%.0f�C',extrema(ii)),'color',colorExtrema(ii))
end
xlabel('Temperature (�C)')
ylabel('Intensity (arb.u.)')
% title('Absorption measurement')
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\figures\Absorption','-dpdf','-r600')

%%
I0=1100;
h=figure;
hold on, box on
plot(Abs250mA(:,1),-log(Abs250mA(:,2)/I0),'d-','color',c(1,:),'MarkerFaceColor',c(1,:))
plot(Abs400mA(:,1),-log(Abs400mA(:,2)/I0),'o-','color',c(2,:),'MarkerFaceColor',c(2,:))
plot(Abs550mA(:,1),-log(Abs550mA(:,2)/I0),'s-','color',c(3,:),'MarkerFaceColor',c(3,:))
legend({'250mA','400mA','550mA'},'location','best')
xlabel('Temperature (�C)')

%% Fluorescence measurement
fluorescenceOff=csvread('fluorescenceOff.csv');
fluorescenceOff2=csvread('fluorescenceOff2.csv');
fluorescenceOn=csvread('fluorescenceOn.csv');

offLimits=[19.2 -20.4];
onLimits=[-18 21.2];

fluorescenceOff(:,2)=fluorescenceOff(:,2)-min(offLimits);
fluorescenceOff2(:,2)=fluorescenceOff2(:,2)-min(offLimits);
fluorescenceOn(:,2)=fluorescenceOn(:,2)-min(onLimits);

expDecay = @(a,b,c,x) a*exp(-1/b*x)+c;
expDecay2Off= @(b,x) expDecay(abs(diff(offLimits)),b,0,x);
expDecay2On= @(b,x) expDecay(-abs(diff(onLimits)),b,abs(diff(onLimits)),x);
expDecay3Off= @(a,b,x) expDecay(a,b,0,x);
expDecay3On= @(a,b,x) expDecay(a,b,abs(diff(onLimits)),x);
fOff=fit(fluorescenceOff(:,1),fluorescenceOff(:,2),expDecay3Off ,'StartPoint',[40 50])%[40 250 0])
fOff2=fit(fluorescenceOff2(:,1),fluorescenceOff2(:,2),expDecay3Off,'StartPoint',[40 50])%[40 250 0])
fOn=fit(fluorescenceOn(:,1),fluorescenceOn(:,2),expDecay3On,'StartPoint',[-40 50])%[40 250 0])

lifetime=[fOff.b fOff2.b fOn.b];
lifetime_range=[confint(fOff,0.68),confint(fOff2,0.68),confint(fOn,0.68)];
x=0:10:600;

c=lines(3);
h=figure;
hold on,box on
xlabel('Time (�s)')
ylabel('Voltage (mV)')
% plot(fluorescenceOff2(:,1),fluorescenceOff2(:,2),'d','MarkerFaceColor',c(2,:))
pOn=errorbar(fluorescenceOn(:,1),fluorescenceOn(:,2),2*ones(size(fluorescenceOn(:,2))),'ro','MarkerFaceColor','r','clipping','off');
plot(x,feval(fOn,x),'color','r')
pOff=errorbar(fluorescenceOff(:,1),fluorescenceOff(:,2),2*ones(size(fluorescenceOff(:,2))),'bs','MarkerFaceColor','b','clipping','off');
plot(x,feval(fOff,x),'color','b')
% plot(x,feval(fOff2,x),'color',c(2,:))
legend([pOn,pOff],{'Turn on','Turn off'},'location','east','box','off')
xlim([0 600])
text(210,36,'\tau = (235 \pm 4) �s','color','r','interpreter','tex');
text(210,4,'\tau = (252 \pm 9) �s','color','b','interpreter','tex');


set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\Fluorescence','-dpdf','-r600')
% figure
% semilogy(fluorescenceOff(:,1),fluorescenceOff(:,2)+16)
% hold on
% semilogy(fluorescenceOff2(:,1),fluorescenceOff2(:,2)+16)
% semilogy(fluorescenceOn(:,1),18-fluorescenceOn(:,2))

%% Temp. dependence Nd:YAG laser
LaserTempDep=csvread('LaserTempDependence_edit.csv',1,0);
extrema=[27 38 44];
colorExtrema=['r','r','b'];
h=figure;
plot(LaserTempDep(:,1),LaserTempDep(:,2),'ks','MarkerFaceColor','k')
for ii=1:3
    line(extrema(ii)*[1 1], [20 LaserTempDep(LaserTempDep(:,1)==extrema(ii),2)],...
        'color',colorExtrema(ii),'linestyle','--','clipping','off')
    text(extrema(ii)-2.5,35,sprintf('%.0f�C',extrema(ii)),'color',colorExtrema(ii))
end
ylim([20 110])
xlabel('Temperature (�C)')
ylabel('Power (mW)')
% hold on
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\LaserTempDependence','-dpdf','-r600')
% plot(Abs550mA(:,1),(max(Abs550mA(:,2))-Abs550mA(:,2))/10,'rs-','MarkerFaceColor','r')

%% Output Nd:Yag laser (absolute units)
Data1064=csvread('LaserClassification.csv',1,0);

p1064=fit(Data1064(end-10:end-1,1),Data1064(end-10:end-1,3),'a*(x-b)');
x=[180 400];
x2=[400 600];
y=feval(p1064,x);
y2=feval(p1064,x2);
onsetNdyag=confint(p1064,0.68);
onsetNdyag=onsetNdyag(:,2);
onsetNdyag=[mean(onsetNdyag),abs(diff(onsetNdyag))/2];

p1064_all=polyfit(Data1064(1:end-1,1),Data1064(1:end-1,3),1);
x_all=[180 600];
y_all=polyval(p1064_all,x_all);

h=figure;
hold on
l1=plot(x_all,y_all,'color',0.8*[1 1 1],'linew',4,'DisplayName','General linear fit');
l2=plot(x,y,'r','linew',1.5,'DisplayName','Fit for low currents');
% plot(x2,y2,'r--','linew',1.5)
l3=plot(Data1064(:,1),Data1064(:,3),'ks','MarkerFaceColor','k','DisplayName','Data');
text(500,75,sprintf('%.2f W/A',p1064_all(1)),'color',0.7*[1 1 1],'rotation',37)
ylim([0 120])
posAx=get(gca,'position');
xl=xlim;
annotation(h,'textarrow',posAx(1)+posAx(3)*(onsetNdyag(1)-xl(1))/diff(xl)*[1 1],[0.30 0.13],...
    'String',{'Threshold:',sprintf('(%.0f \\pm %.0f) mA',onsetNdyag)},'color','r','HorizontalAlignment','center');
xlabel('Current (mA)')
ylabel('Power (mW)')
% title('Nd:YAG laser output')

ax1=gca;
ax1.ClippingStyle = 'rectangle';
ax2=axes('Position',ax1.Position,'XAxisLocation','top','YAxisLocation','right',...
    'YTick',35:2:45,'XTickLabel','','Color','none');
ax2.YColor = 'b';
hold on
l4=plot(Data1064(:,1),Data1064(:,2),'bd','MarkerFaceColor','b','MarkerSize',5,'DisplayName','Set diode temp.');
ylabel('Temperature (�C)')
ylim([36 42])
% legend({},'location','best')
legend([l3 l1 l2 l4],'location','north')
linkaxes([ax1,ax2],'x')

set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\NdYAG-Output','-dpdf','-r600')
%%
figure
plot(interp1(DataDiode(:,1),DataDiode(:,3),Data1064(:,1)),Data1064(:,3),'ks-','MarkerFaceColor','k')
xlabel('Pump power (mW)')
ylabel('Output power (mW)')

fTemp=fit(Data1064(1:20,1),Data1064(1:20,2),'poly1');
figure
hold on, box on
% plot(x,y,'r','linew',1.5)
plot(fTemp,Data1064(1:20,1),Data1064(1:20,2))%,'bs','MarkerFaceColor','b')
xlabel('Current (mA)')
ylabel('Temperature (�C)')
title('Nd:YAG laser output')

%% Output green Nd:Yag laser (absolute units)
DataSHG=csvread('SHG.csv',1,0);
outlyer=DataSHG(:,1)>480;
pSHG=fit(DataSHG(:,1),DataSHG(:,2),'a*(x-b)^2','StartPoint',[1 250],'Exclude',outlyer);
x=pSHG.b:580;
y=feval(pSHG,x);

onsetSHG=confint(pSHG,0.68);
onsetSHG=onsetSHG(:,2);
onsetSHG=[mean(onsetSHG),abs(diff(onsetSHG))/2];

h=figure;
hold on, box on
errorbar(DataSHG(:,1),DataSHG(:,2),0.1*DataSHG(:,2),'ks','MarkerFaceColor','k','MarkerSize',4)
plot(DataSHG(outlyer,1),DataSHG(outlyer,2),'rs','MarkerFaceColor','r','MarkerSize',4)
plot(x,y,'k-')
xlim([170 570])
posAx=get(gca,'position');
xl=xlim;
annotation(h,'textarrow',posAx(1)+posAx(3)*(pSHG.b-xl(1))/diff(xl)*[1 1],[0.30 posAx(2)],...
    'String',{'Threshold:',sprintf('(%.0f \\pm %.0f) mA',onsetSHG)},'color','k','HorizontalAlignment','center');
xlabel('Current (mA)')
ylabel('SHG output (mW)')
legend({'Data','Data excluded from fit','Parabolic fit'},'location','best')
% title('Frequency doubled Nd:YAG output')
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\SHG-Output','-dpdf','-r600')
%%
fSHG2=fit(interp1(DataDiode(:,1),DataDiode(:,3),DataSHG(:,1)),DataSHG(:,2),'a*(x-b)^2','StartPoint',[1 0.3]);
x2=100:400;
y2=feval(fSHG2,x2);

figure
hold on, box on
plot(interp1(DataDiode(:,1),DataDiode(:,3),DataSHG(:,1)),DataSHG(:,2),'ks','MarkerFaceColor','k')
plot(x2,y2,'k-')
xlabel('Pump power (mW)')
ylabel('SHG output (mW)')
% title('Frequency doubled Nd:YAG output')

% figure
% loglog(DataSHG(:,1)-240,DataSHG(:,2),'ks-','MarkerFaceColor','k')
% xlabel('Current (mA)')
% ylabel('SHG output (mW)')
