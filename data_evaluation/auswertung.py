import numpy as np
import matplotlib.pyplot as plt
import scipy

# Temp 20.2 deg C, [Current [10 mA], Intensity [a.u.]]
# Controller Gain: 5, Outtput Display gain: 10
intensity20deg = np.array([[0,0],[2,0],[4,0],[6,0],[8,0],[9,1],[12,1],[14,1],[16,21],
        [18,95],[20,162],[22,223],[24,282],[26,335],[28,385],[30,433],[32,477],
        [34,517],[36,559],[38,598],[40,636],[42,676],[44,716],[46,758],[48,798],
        [50,837],[52,877],[54,920],[56,962]])


#plt.plot(intensity20deg[:,0]*10,intensity20deg[:,1])
#plt.show()





# Absorption in Nd-YAG Crystal
# Current 550 mA, [Temp [deg C], Intensity [a.u.]]
# Controller Gain: 5, Outtput Display gain: 1
intensityAbsorp550mA = np.array([[5, 841],[6,954],[7,986],[8,971],[9,1010],[10,1014],[11,1017],
    [12,1044],[13,1057],[14,1003],[15,900],[16,917],[17,931],[18,645],[19,598],[20,649],
    [21,367],[22,215],[23,291],[24,264],[25,157],[26,135],[27,189],[28,218],[29,208],
    [30,187],[31,204],[32,207],[33,170],[34,111],[35,120],[35.5,143],[36,120],[36.5,80],
    [37,39],[37.5,48],[38,60],[38.5,70],[39,87],[39.5,98],[40,92],[40.5,74],[41,60],
    [41.5,60],[42,159],[42.5,310],[43,395],[43.5,355],[44,335]])#

intensityAbsorp400mA = np.array([[45,115],[44,143],[43.5,169],[43,175],[42.5,91],[42,28],
    [41.5,23],[41,21],[40.5,23],[40,28],[39.5,49],[39,58],[38.5,63],[38,60],[37.5,53],[37,51]])#

# Controller Gain: 10, Outtput Display gain: 1
intensityAbsorp250mA = np.array([[36,336],[36.5,320],[37,227],[37.5,209],[38,158],[38.5,220],
    [39,237],[39.5,238],[40,130],[40.5,87],[41,92],[41.5,103],[42,120],[42.5,100],[43,110],[43.5,77],[44,90],[44.5,58],[45,47]])#
#print(intensityAbsorp250mA);exit()

#plt.plot(intensityAbsorp550mA[:,0],1-(intensityAbsorp550mA[:,1]/np.max(intensityAbsorp550mA[:,1])),'x-')
#plt.plot(intensityAbsorp400mA[:,0],1-(intensityAbsorp400mA[:,1]/np.max(intensityAbsorp400mA[:,1])),'x-')
#plt.plot(intensityAbsorp250mA[:,0],1-(intensityAbsorp250mA[:,1]/np.max(intensityAbsorp250mA[:,1])),'x-')
#plt.show()






# Fluoreszenzmessung

# time [s], mV

# delta U = 19.2mV
fluorescenceOff = np.array([[4e-6,0],[80e-6,8.8],[160e-6,18],[240e-6,24],[320e-6,28.4],[400e-6,32],[480e-6,34]])
fluorescenceOff[:,0]=fluorescenceOff[:,0]*1e6
fluorescenceOff[:,1]=-fluorescenceOff[:,1]+19.2

#messung in 50 ms Schritten, startend bei 0ms
fluorescenceOff2=np.array([19.2,13.6,7.2,2.4,-2,-5.6,-8.4,-10.4,-12.4])#
time=np.linspace(0,50*len(fluorescenceOff2), len(fluorescenceOff2))
fluorescenceOff2=np.vstack((time,fluorescenceOff2)).T

# delta U = -18mV
fluorescenceOn = np.array([[4.2e-3,-18],[4.28e-3,-6],[4.36e-3,1.2],[4.44e-3,6.8],[4.52e-3,11.2],[4.6e-3,14],[4.68e-3,16.4],[4.76e-3,17.6]])#
fluorescenceOn[:,0]=(fluorescenceOn[:,0]-fluorescenceOn[0,0])*1e6

plt.plot(fluorescenceOff[:,0],fluorescenceOff[:,1])
plt.plot(fluorescenceOff2[:,0],fluorescenceOff2[:,1])
plt.plot(fluorescenceOn[:,0],fluorescenceOn[:,1])
plt.show()

# output
np.savetxt('fluorescenceOff.csv',fluorescenceOff, delimiter=',') 
np.savetxt('fluorescenceOff2.csv',fluorescenceOff2, delimiter=',') 
np.savetxt('fluorescenceOn.csv',fluorescenceOn, delimiter=',') 


# Spiking
# photo beginning: 250mA pump current
# photo end: 590mA




# SHG
# Wenn Spigel ersetzt, dann Leistung von 9.2mW




