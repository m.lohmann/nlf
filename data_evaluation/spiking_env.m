%TODO: axes labels, nice curves, nice axes ticks, go to pdf check good,
%optimize position distance between single plots
%linewidth (in pdf with \lipsum)


clear all;
close all;
thres=220;
first=250;
sec=590;

%b=[0.001,0.001,0.001];
%p=[3*b,6*b,12*b];
%dt=0.005e-1;

b=[0.001,0.001,0.001,0.05,0.05,0.05];
pfac=[2,4,8,2,4,8];
p=pfac.*b;
dt=[0.004,0.006,0.004,0.02,0.02,0.02]*1e-1;
tmax=[4500,4500,4500,500,500,500];


dampt=2./p/sqrt(2);% 6,12,24 b

% compare theory values for oscillation freq and damping coefficient after
% bringing equation from p. 309 in n,q,p,b form. 





for i=1:length(p)
    p(i)
    relax_theo(i)=1./(b(i).*sqrt(1./b(i).*(p(i)./b(i)-1)-(p(i)./(2.*b(i))).^2))*2*pi;
    
    %plot(t(:,i),n(:,i),'r:');
    [t{i},n{i},q{i}]=LaserRateEquations_tom(b(i),p(i),dt(i),tmax(i));
    relax_theo(i)
    [pks{i},pks_frame{i}]=findpeaks(q{i},'MinPeakDistance',round(relax_theo(i)*0.7/dt(i)),'MinPeakHeight',0.01*max(q{i})); 
    
    [pksmin{i},pks_framemin{i}]=findpeaks(-q{i}+max(q{i}),'MinPeakDistance',round(relax_theo(i)*0.7/dt(i)),'MinPeakHeight',0.01*max(q{i})); 
    onlyafter=pks_framemin{i}>(pks_frame{i}(1));
    pks_framemin{i}=pks_framemin{i}(onlyafter);
    pksmin{i}=-pksmin{i}(onlyafter)+max(q{i});
    
    %figure(23)
    %plot(pks/max(pks));
    %hold on;
end
 

%xlabel('Time')
%ylabel('Photon number $q$')

%ylabel(AX(2),'Population difference $n$')
%text(280,0.165,sprintf('$p=%.2f, b=%.2f$',p,b),'FontSize',12,'interpreter','latex')
%text(340,0.015,'$q$','FontSize',12,'Color',ax1.Color,'interpreter','latex')
%text(340,1.05,'$n$','FontSize',12,'Color',ax2.Color,'Parent', AX(2),'interpreter','latex')
%set(ax2,'LineStyle','--')

%%
close all
spik_fig=figure(22);
set(0,'defaultaxesfontsize',8.5);
set(0,'defaulttextfontsize',8.5); 
set(spik_fig,'Units','centimeters');
set(spik_fig,'Position',[4,4,15,16]);
plot_ind=[1,3,5,2,4,6];

size=[0.34,0.25];
left=0.08;
positions={[left,0.71,size];[left,0.39,size];[left,0.08,size];...
    [left+0.5,0.71,size];[left+0.5,0.39,size];[left+0.5,0.08,size]}
colq=[0,0,0.6];
coln=[0.7,0.3,0];
ticks={[0 0.002 0.004 0.006],[0 0.005,0.01,0.015],[0 0.01,0.02,0.03,0.04],[0 0.02,0.04,0.06,0.08,0.1,0.12],[0 0.1,0.2,0.3,0.4],[0 0.2,0.4,0.6,0.8]};
lims={[0 4500];[0 4500];[0 4500];[0 150];[0 150];[0 150]};
clear fitcoeff;
for i=1:length(p)
    figure(22)
    subplot('Position',positions{i});
   

    [ax1,ax2,ax3]=plotyy(t{i},q{i},t{i},n{i});
    set(ax2(1),'Color',colq,'LineWidth',1);
    set(ax1(1),'YColor',colq);
    
    set(ax3(1),'Color',coln,'LineStyle','-','LineWidth',0.75);
    set(ax1(2),'YColor',coln);
    hold on;
    
    ylim(ax1(1),[-0.02*max(q{i}),1.07*max(q{i})]);
    ylim(ax1(2),[-0.02*max(n{i}),1.07*max(n{i})]);
    yticks(ax1(2),[0,1,2,3,4,5]);
    yticks(ax1(1),ticks{i});
    text(ax1(1),0.55*lims{i}(2),0.97*max(q{i}),sprintf('b=%.3f, p=%.0fb',b(i),pfac(i)),'Interpreter','latex');
   
    %scatter(t(pks_frame{i},i),pks{i});
    %scatter(t(pks_framemin{i},i),pksmin{i});
    expDecay = @(a,b,c,x) a*exp(-1/b.*(x))+c;
    pkfit=fit(t{i}(pks_frame{i}),pks{i},expDecay,'Startpoint',[0.4,dampt(i),0.004])
    fitcoeff(:,i)=coeffvalues(pkfit);
    damp(i)=fitcoeff(2,i);
    yc=feval(pkfit,t{i});
    %text(ax1(1),t{i}(pks_frame{i}(1))+2*damp(i),0.4*pks{i}(1),sprintf('\\tau=%.0f',damp(i)),'Interpreter','latex');
    fitp=plot(t{i},yc,'k:');
    set(fitp,'LineWidth',1);
    
    xlim(ax1,lims{i});
    ylabel(ax1(1),'Photon number \it q ','Interpreter','tex');
    ylabel(ax1(2),'Population difference \it n','Interpreter','tex');
   
    if(i==3 | i==6)
        xlabel(ax1(1),'Time \it t','Interpreter','tex');
    end
    
end
%xlim([0 400]);

pos = get(spik_fig,'Position');
set(spik_fig,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[1.01*pos(3), 1.01*pos(4)])
print(spik_fig,'.\protocol_thomas\figures\spiking_simu','-dpdf','-r600')
%%
for i=1:length(p)
    pdist{i}=diff(t{i}(pks_frame{i}));
    figure(88)
    plot(pdist{i});
    hold on;
    relax_t(i)=mean(pdist{i}(2:end));
    
    rel_damp(i)=relax_t(i)/fitcoeff(2,i);
end