report submitted on 03/19/19.

old stuff:
THINGS TO STILL DO:
-> kinking in diode laser character. theory.
-> Quantum efficiency for DIODE
-> captions and references in SHG chapter. 
-> picture for laser components, rate eq. like in Johns pap. / instruct.
-> error margins in nd:yag fits (thresh fill and slope fill (both?)).
-> replace \alpha in all the regressions with different case specific variable names.
-> i-i0 in pump diode charac. 

THINGS TO CHECK IN THE END:
- I_on/I_th
- sansserif in all figures?!
- all formulas (especially regressions if make sense !!)
- citations in theory


QUESTIONS / discuss with felix:
-> absorption spectrum errors in power?!
-Setup/Meas:
-> exact positions written down?


Notes for Nd:Yag characteriz.:
-> although the dependency is not linear, linear assumed in order to calculate the average slope efficiency of the complete laser, which is obtained as ?? W/A.
